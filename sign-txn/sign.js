// vars
const FROM_ACCOUNT = "0x64Cc77dC602ea7215721e8f12d737b6deB827943"; // must be whitelisted
const FROM_ACCOUNT_PRIVATE_KEY = "595C67330C9205717B3F4B763EB46E1C250EA837A69D99E51FD39DD4A40CB302"
const TO_ACCOUNT = "0x5B8aE8E9BEDD74af6D11F66A34c68fdED4187fF6";  // must be whitelisted
const TOKEN_ADDRESS = "0xF12D72b2F022F5335d79528E725986c1Ea1BfaFe";
const NODE_URL = "http://dev-blockchain.panzerpay.com:8540";


// libs
const Web3 = require('web3')
const Tx = require('ethereumjs-tx')
const fs = require('fs')


const TOKEN_ABI = JSON.parse(fs.readFileSync('token-abi.json', 'utf-8'));


const web3 = new Web3(new Web3.providers.HttpProvider(NODE_URL))

web3.eth.getTransactionCount(FROM_ACCOUNT).then(function (count) {
    var contract = new web3.eth.Contract(TOKEN_ABI, TOKEN_ADDRESS);
    var rawTransaction = {
        "from": FROM_ACCOUNT,
        "nonce": web3.utils.toHex(count),
        "gasPrice": web3.utils.toHex('0'),
        "gasLimit": web3.utils.toHex('50000'),
        "to": TOKEN_ADDRESS,
        "value": "0x0",
        "data": contract.methods.transfer(TO_ACCOUNT, 1, "orderid", "remarks").encodeABI(), // amount = 1 * (10^-18)
        "chainId": 0x2323
    };

    var privKey = new Buffer(FROM_ACCOUNT_PRIVATE_KEY, 'hex');
    var tx = new Tx(rawTransaction);

    tx.sign(privKey);
    var serializedTx = tx.serialize();

    console.log('0x' + serializedTx.toString('hex'));
    // web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (err, hash) {
    //     if (!err)
    //         console.log(hash);
    //     else
    //         console.log(err);
    //     return;
    // });
});
